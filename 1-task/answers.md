### Задание 1. Yandex Cloud 

**Что нужно сделать**

1. Создать пустую VPC. Выбрать зону.
2. Публичная подсеть.

 - Создать в VPC subnet с названием public, сетью 192.168.10.0/24.
 - Создать в этой подсети NAT-инстанс, присвоив ему адрес 192.168.10.254. В качестве image_id использовать fd80mrhj8fl2oe87o4e1.
 - Создать в этой публичной подсети виртуалку с публичным IP, подключиться к ней и убедиться, что есть доступ к интернету.
3. Приватная подсеть.
 - Создать в VPC subnet с названием private, сетью 192.168.20.0/24.
 - Создать route table. Добавить статический маршрут, направляющий весь исходящий трафик private сети в NAT-инстанс.
 - Создать в этой приватной подсети виртуалку с внутренним IP, подключиться к ней через виртуалку, созданную ранее, и убедиться, что есть доступ к интернету.

 ### Ответы:

 - [public vm](https://gitlab.com/mamaiifortesting/cloud-infra/-/blob/main/1-task/infra/public.tf?ref_type=heads)
 - [private vm](https://gitlab.com/mamaiifortesting/cloud-infra/-/blob/main/1-task/infra/private.tf?ref_type=heads)
 - [vpc/subnetwork/RT](https://gitlab.com/mamaiifortesting/cloud-infra/-/blob/main/1-task/infra/private.tf?ref_type=heads)
 - [nat instance](https://gitlab.com/mamaiifortesting/cloud-infra/-/blob/main/1-task/infra/nat-instance.tf?ref_type=heads)
 - Итоговый вариант пинга с приватной ВМ

 ![alt text](image.png)

