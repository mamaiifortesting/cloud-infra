###global var
variable "token" {
  type        = string
  description = "OAuth-token; https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token"
}

variable "cloud_id" {
  type        = string
  description = "https://cloud.yandex.ru/docs/resource-manager/operations/cloud/get-id"
}

variable "folder_id" {
  type        = string
  description = "https://cloud.yandex.ru/docs/resource-manager/operations/folder/get-id"
}

variable "default_zone" {
  type        = string
  default     = "ru-central1-a"
  description = "https://cloud.yandex.ru/docs/overview/concepts/geo-scope"
}
variable "cidr" {
  type          = map(object({
    v4_cidr_blocks = list(string)
  }))
  default = {
    public  = { v4_cidr_blocks = ["192.168.10.0/24"] }
    private = { v4_cidr_blocks = ["192.168.20.0/24"] }
  }
}

variable "vpc_name" {
  type        = string
  default     = "netology-vpc"
  description = "netology-vpc"
}

variable "vm_os" {
  type        = string
  default     = "ubuntu-2004-lts-oslogin"
}

### vm vars
variable "vms_resources" {
  type        = map(object({
    cores         = number
    memory        = number
    core_fraction = number
  }))
}

variable "vm_platform_id" {
  type        = string
  default     = "standard-v3"
}

