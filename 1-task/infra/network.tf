resource "yandex_vpc_network" "netology_vpc" {
  name = var.vpc_name
}
resource "yandex_vpc_subnet" "netology_vpc" {
  for_each       = var.cidr
  name           = "subnet-${each.key}"
  zone           = var.default_zone
  network_id     = yandex_vpc_network.netology_vpc.id
  v4_cidr_blocks = each.value.v4_cidr_blocks
}

resource "yandex_vpc_route_table" "nat-instance-route" {
  name       = "nat-instance-route"
  network_id = yandex_vpc_network.netology_vpc.id
  static_route {
    destination_prefix = "0.0.0.0/0"
    next_hop_address   = yandex_compute_instance.nat-instance-ubuntu.network_interface.0.ip_address
  }
}

