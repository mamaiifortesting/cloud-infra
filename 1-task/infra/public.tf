data "yandex_compute_image" "public_vm" {
  family = var.vm_os
}

resource "yandex_compute_instance" "public_vm" {
  name        = "ubuntu-public"
  platform_id = var.vm_platform_id
  resources {
    cores         = var.vms_resources["web"]["cores"]
    memory        = var.vms_resources["web"]["memory"]
    core_fraction = var.vms_resources["web"]["core_fraction"]
  }
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.public_vm.image_id
    }
  }
  scheduling_policy {
    preemptible = true
  }
  network_interface {
    subnet_id          = yandex_vpc_subnet.netology_vpc["public"].id
    nat                = true
  }
  metadata = {
    serial-port-enable = 1
    ssh-keys           = "ubuntu:${local.ssh_public_key}"
  }
}
