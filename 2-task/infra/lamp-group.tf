resource "yandex_compute_instance_group" "lamp_group" {
  name                = "lamp-group"
  service_account_id  = "${yandex_iam_service_account.sa.id}"
  folder_id           = var.folder_id
  depends_on = [yandex_storage_object.image]
  instance_template {
    platform_id = var.vm_platform_id
    resources {
      cores  = 2
      memory = 2
    }
    boot_disk {
      initialize_params {
        image_id = "fd827b91d99psvq5fjit"
      }
    }
    network_interface {
      network_id = "${yandex_vpc_network.netology_vpc.id}"
      subnet_ids = ["${yandex_vpc_subnet.netology_vpc.id}"]
      nat        = true
    }
    metadata = {
      serial-port-enable = 1
      ssh-keys           = "ubuntu:${local.ssh_public_key}"
      user-data          = <<-EOF
        #!/bin/bash
        sudo apt-get update
        sudo apt-get install -y apache2
        sudo systemctl start apache2
        sudo systemctl enable apache2
        echo "<html><body><h1>Welcome to LAMP</h1><img src='https://storage.yandexcloud.net/${yandex_storage_bucket.image_bucket.bucket}/image.jpg'></body></html>" | sudo tee /var/www/html/index.html
      EOF
    }
  }
  scale_policy {
    fixed_scale {
      size = 3
    }
  }
  allocation_policy {
    zones = ["ru-central1-a"]
  }
  deploy_policy {
    max_unavailable = 1
    max_expansion   = 1
  }
  health_check {
    interval = 15
    timeout  = 10
    healthy_threshold   = 2
    unhealthy_threshold = 2
    http_options {
      path = "/"
      port = 80
    }
  }
}
