resource "yandex_lb_target_group" "foo" {
  name      = "tg-ig"
  region_id = "ru-central1"

  target {
    subnet_id = yandex_vpc_subnet.netology_vpc.id
    address   = yandex_compute_instance_group.lamp_group.instances[0].network_interface.0.ip_address
  }

  target {
    subnet_id = yandex_vpc_subnet.netology_vpc.id
    address   = yandex_compute_instance_group.lamp_group.instances[1].network_interface.0.ip_address
  }

  target {
    subnet_id = yandex_vpc_subnet.netology_vpc.id
    address   = yandex_compute_instance_group.lamp_group.instances[2].network_interface.0.ip_address
  }
}

resource "yandex_lb_network_load_balancer" "lb" {
  name = "test-lb"

  listener {
    name = "http"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_lb_target_group.foo.id}"

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}