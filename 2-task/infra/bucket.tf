resource "yandex_storage_bucket" "image_bucket" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = "test-bucket-${var.student_name}-${var.date}"
  acl    = "public-read"
  depends_on = [yandex_resourcemanager_folder_iam_member.sa-editor]
}

resource "yandex_storage_object" "image" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = "test-bucket-${var.student_name}-${var.date}"
  key    = "image.jpg"
  source = "/home/ubuntu/netology/image.jpg"
  depends_on = [yandex_storage_bucket.image_bucket]
}
