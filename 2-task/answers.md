## Задание 1. Yandex Cloud 

**Что нужно сделать**

1. Создать бакет Object Storage и разместить в нём файл с картинкой:

 - Создать бакет в Object Storage с произвольным именем (например, _имя_студента_дата_).
 - Положить в бакет файл с картинкой.
 - Сделать файл доступным из интернета.

### Ответ:

 - [Манифест с бакетом и объектом](https://gitlab.com/mamaiifortesting/cloud-infra/-/blob/main/2-task/infra/bucket.tf?ref_type=heads). После добавления ключей в объект - бакет создался без проблем.

 ![alt text](image.png)
 
2. Создать группу ВМ в public подсети фиксированного размера с шаблоном LAMP и веб-страницей, содержащей ссылку на картинку из бакета:

 - Создать Instance Group с тремя ВМ и шаблоном LAMP. Для LAMP рекомендуется использовать `image_id = fd827b91d99psvq5fjit`.
 - Для создания стартовой веб-страницы рекомендуется использовать раздел `user_data` в [meta_data](https://cloud.yandex.ru/docs/compute/concepts/vm-metadata).
 - Разместить в стартовой веб-странице шаблонной ВМ ссылку на картинку из бакета.
 - Настроить проверку состояния ВМ.

### Ответ:

 - [Манифест с группой ВМ](https://gitlab.com/mamaiifortesting/cloud-infra/-/blob/main/2-task/infra/lamp-group.tf?ref_type=heads). Проблем с созданием не было.
 
 ![alt text](image-1.png)

3. Подключить группу к сетевому балансировщику:

 - Создать сетевой балансировщик.
 - Проверить работоспособность, удалив одну или несколько ВМ.

### Ответ:

 - [Манифест с ЛБ и группой для ЛБ](https://gitlab.com/mamaiifortesting/cloud-infra/-/blob/main/2-task/infra/lb.tf?ref_type=heads). Проблема с созданием балансера была из-за криво указанной target_group_id в блоке attached_target_group. Я указывал создаваемый ресурс из группы ВМ, а оно так не умеет (как ни странно).

 ![alt text](image-2.png)

 ### Итоговый вариант:

  - Всё прекрасно)

  ![alt text](image-3.png)