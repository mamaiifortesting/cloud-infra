resource "yandex_vpc_network" "netology_vpc" {
  name = var.vpc_name
}
resource "yandex_vpc_subnet" "netology_vpc" {
  name           = "netology-subnet"
  zone           = var.default_zone
  network_id     = yandex_vpc_network.netology_vpc.id
  v4_cidr_blocks = var.default_cidr
}

