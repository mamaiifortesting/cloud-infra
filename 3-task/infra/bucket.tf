resource "yandex_kms_symmetric_key" "key-a" {
  name              = "test-key"
  description       = "description for key"
  default_algorithm = "AES_128"
  rotation_period   = "8760h" // equal to 1 year
}

resource "yandex_storage_bucket" "image_bucket" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = "test-bucket-${var.student_name}-${var.date}"
  acl    = "public-read"
  depends_on = [yandex_resourcemanager_folder_iam_member.sa-editor]
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = yandex_kms_symmetric_key.key-a.id
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

resource "yandex_storage_object" "image" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = "test-bucket-${var.student_name}-${var.date}"
  key    = "image.jpg"
  source = "/home/ubuntu/netology/image.jpg"
  depends_on = [yandex_storage_bucket.image_bucket]
}

